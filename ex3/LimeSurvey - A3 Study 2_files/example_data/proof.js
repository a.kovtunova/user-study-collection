import {APP_GLOBALS as app, SharedData} from "./sharedData.js";
import {AxiomFunctionsHelper} from "./axiomFunctions.js";

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const proofState = urlParams.get("state");

// Configure SVG
app.svg = d3.select("#proof-view");
app.BBox = app.svg.node().getBoundingClientRect();
app.SVGwidth = app.BBox.width;
app.SVGheight = app.BBox.height;
app.margin = {top: 30, right: 10, bottom: 30, left: 10};
app.contentWidth = app.SVGwidth - app.margin.left - app.margin.right;
app.contentHeight = app.SVGheight - app.margin.top - app.margin.bottom;
app.svg
  .attr("viewBox", [-app.margin.left, -app.margin.top, app.SVGwidth, app.SVGheight])
  .style("user-select", "none");
app.svgRootLayer = d3.select("#svgRootLayer");

// Configure Shared Data
SharedData.axiomFunctionsHelper = new AxiomFunctionsHelper();
//Update the width of the proof
app.proofWidth = app.contentWidth;
//Update the height of the proof
app.proofHeight = app.contentHeight;
// Configure Windows resizing
window.addEventListener("resize", () => SharedData.advancedUpdate());
//Load the proof file
d3.xml(proofFileName).then((xml) => {
    createContent(xml);
});

// Initialize navigation features (zooming and panning)
initPanZoom();
/**
 * Initializes the pan-and-zoom library.
 *
 * For further options see https://github.com/ariutta/svg-pan-zoom.
 */
function initPanZoom() {
  const svgView = document.querySelector('svg#proof-view');
  const panZoomCtrl = svgPanZoom(svgView, {
    viewportSelector: '.svg-pan-zoom_viewport',
    controlIconsEnabled: true,
    dblClickZoomEnabled: false,
    mouseWheelZoomEnabled: false
  });
}

// define arrowheads
let arrowPoints = [[0, 0], [0, 20], [20, 10]];
app.svg.append('defs')
    .append('marker')
    .attr('id', 'arrowhead')
    .attr('viewBox', [0, 0, 20, 20])
    .attr('refX', 26.5)
    .attr('refY', 10)
    .attr('markerWidth', 8)
    .attr('markerHeight', 8)
    .attr('orient', 'auto-start-reverse')
    .append('path')
    .attr('d', d3.line()(arrowPoints))
    .attr('fill', 'darkgrey');

function createContent(data) {
  // Generate nodes and edges from the raw data
  let processedData = processData(data);
  let nodeData = processedData.nodes;
  let edgeData = processedData.edges;

  edgeData.push({
    id: "L-1",
    source: nodeData[0],
    target: ""
  });
  //Store original data
  SharedData.edgeData = edgeData;
  SharedData.hierarchy = SharedData.createHierarchy(edgeData);

  // update and draw the tree
  SharedData.updateHierarchyVars(SharedData.hierarchy);

  SharedData.links = app.svgRootLayer.append("g")
      .attr("id", "links")
      .attr("cursor", "pointer")
      .attr("pointer-events", "all");

  SharedData.nodes = app.svgRootLayer.append("g")
      .attr("id", "nodes")
      .attr("cursor", "pointer")
      .attr("pointer-events", "all");

  SharedData.labels = app.svg.selectAll("#nodes");

  SharedData.advancedUpdate(SharedData.hierarchy);

  if (proofState !== "full")
    SharedData.axiomFunctionsHelper.showConclusionOnly();
}

function processData(data) {
  // Compute edges
  let edgeData = [].map.call(data.querySelectorAll("edge"), d => {
    let edgeId = d.getAttribute("id");
    let edgeSource = d.getAttribute("source");
    let edgeTarget = d.getAttribute("target");

    return { id: edgeId, source: edgeSource, target: edgeTarget };
  });

  // Compute nodes
  let nodeData = [].map.call(data.querySelectorAll("node"), d => {
    let dataNodes, typeVar, elementVar, msElementVar, nlElementVar, idVar;

    idVar = d.getAttribute("id");

    dataNodes = d.querySelectorAll("data");

    dataNodes.forEach(function (item) {
      if (item.getAttribute("key") === "type") {
        typeVar = item.textContent;
      }
      else if(item.getAttribute("key") === "nlelement"){
        nlElementVar = item.textContent;
      }
    });
    return { id: idVar, type: typeVar,nlelement:nlElementVar};
  });

  // Add the nodeData to the edgeData
  edgeData.forEach(function (d) {

    var sourceNode = nodeData.find(b => b.id === d.source);
    var targetNode = nodeData.find(b => b.id === d.target);

    d.source = sourceNode;
    d.target = targetNode;
  });

  return {
    nodes: nodeData,
    edges: edgeData
  };
}