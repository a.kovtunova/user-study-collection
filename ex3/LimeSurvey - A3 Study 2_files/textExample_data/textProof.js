import {APP_GLOBALS as app, SharedData} from "./textSharedData.js";

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const proofState = urlParams.get("state");

// Configure SVG
app.svg = d3.select("#proof-view");
app.BBox = app.svg.node().getBoundingClientRect();
app.SVGwidth = app.BBox.width;
app.SVGheight = app.BBox.height;
app.margin = {top: 30, right: 10, bottom: 30, left: 10};
app.contentWidth = app.SVGwidth - app.margin.left - app.margin.right;
app.contentHeight = app.SVGheight - app.margin.top - app.margin.bottom;
app.svg
  .attr("viewBox", [-app.margin.left, -app.margin.top, app.SVGwidth, app.SVGheight])
  .style("user-select", "none");
app.svgRootLayer = d3.select("#svgRootLayer");
app.proofWidth = app.contentWidth;
app.proofHeight = app.contentHeight;
app.proofLineCharCount = 10;

//Navigation
d3.select("#navigation-container")
    .style("left",app.SVGwidth/2 - navOffsetLeft+"px")
    .style("top", navOffsetTop*app.SVGheight/4+"px")

//Next button
d3.select("#btn-next").classed("disableBtn", false);
d3.select("#btn-next").classed("enableBtn", true);
d3.select("#btn-next").on("click", function() {
  resetColor();
  showNext();
  updateButtonsState();
})

function showNext(){
  let current;
  let i = 0;
  for(i; i<SharedData.orderedElements.length; i++){
    current = d3.select("#N"+SharedData.orderedElements[i].data.source.id)
    if(current.classed("invisible")){
      current.classed("invisible", false);
      current.classed("visible", true);
      return;
    }
  }
}

//Previous button
d3.select("#btn-previous").on("click", function() {
  resetColor();
  hideCurrent();
  updateButtonsState();
});

function hideCurrent(){
  let current, rev = [...SharedData.orderedElements].reverse();
  let i = 0;
  for(i; i< rev.length -1 ; i++){
    current = d3.select("#N"+rev[i].data.source.id)
    if(current.classed("visible")){
      current.classed("invisible", true);
      current.classed("visible", false);
      return;
    }
  }
}

function resetColor(){
  let current;
  let i = 0;
  for(i; i<SharedData.orderedElements.length; i++){
    current = d3.select("#N"+SharedData.orderedElements[i].data.source.id);
    current.select(".axiomLabel").classed("premiseSentence", false);
    current.select(".axiomLabel").classed("conclusionSentence", false);
    current.select("#frontRect").attr("visibility", "hidden");
  }
}

//Highlight button
d3.select("#btn-highlight").on("click", function() {
  // highlightCurrentRule();
});

function highlightCurrentRule(){
  let current = findCurrent();
  highlightConclusion(current);
  highlightPremise(current);
}

function highlightPremise(object){
  object.each(d=>{
    d.children?.forEach(c=>{
      d3.select("#N" + c.data.source.id).select(".axiomLabel").classed("premiseSentence", true);
    })
  })
}

function highlightConclusion(object){
  object.select(".axiomLabel").classed("conclusionSentence", true);
}

function findCurrent() {
  let i = 0,current,next;
  for(i; i < SharedData.orderedElements.length; i++){
    current = d3.select("#N" + SharedData.orderedElements[i].data.source.id);
    if(i === SharedData.orderedElements.length-1)
      return current;

    next = d3.select("#N" + SharedData.orderedElements[i+1].data.source.id);
    if(current.classed("visible") && next.classed("invisible"))
      return current;
  }
}

function updateButtonsState(){
  let buttons = [d3.select("#btn-previous"),d3.select("#btn-next")];
  let first = SharedData.orderedElements[0];
  let last = SharedData.orderedElements[SharedData.orderedElements.length-1];
  let current = findCurrent().data()[0];

  if(first === current)
    disableButton(buttons[0]);
  else
    enableButton(buttons[0]);
  
  if(last === current)
    disableButton(buttons[1]);
  else
    enableButton(buttons[1]);
}

function disableButton(element){
  element.classed("enableBtn", false)
  element.classed("disableBtn", true)
}

function enableButton(element){
  element.classed("disableBtn", false)
  element.classed("enableBtn", true)
}

//lock button
d3.select("#btn-lock").on("click", function() {
  app.padLock = !app.padLock;
  disableEnableDrag();
});

function disableEnableDrag(){
  if(app.padLock)
    disableDrag();
  else
    enableDrag();
}

function disableDrag(){
  d3.select("#btn-lock").select("i").text("lock_outline");
  d3.select("#navigation-container").classed("draggable", false);
}

function enableDrag(){
  d3.select("#btn-lock").select("i").text("lock_open");
  d3.select("#navigation-container").classed("draggable", true);
}

// Configure Windows resizing
window.addEventListener("resize", () => SharedData.advancedUpdate());

d3.text(parCountFileName).then(function(text){
  SharedData.endOfPars = [];
  d3.csvParseRows(text).forEach(x=>{
    SharedData.endOfPars.push(parseInt(x))
  });
  d3.xml(proofFileName).then((xml) => {
    createContent(xml);
    updateButtonsState();
  });
});


// Initialize navigation features (zooming and panning)
initPanZoom();

/**
 * Initializes the pan-and-zoom library.
 *
 * For further options see https://github.com/ariutta/svg-pan-zoom.
 */
function initPanZoom() {
  const svgView = document.querySelector('svg#proof-view');
  const panZoomCtrl = svgPanZoom(svgView, {
    viewportSelector: '.svg-pan-zoom_viewport',
    controlIconsEnabled: true,
    dblClickZoomEnabled: false,
    mouseWheelZoomEnabled: false
  });
}

function createContent(data) {
  // Generate nodes and edges from the raw data
  let processedData = processData(data);
  let nodeData = processedData.nodes;
  let edgeData = processedData.edges
  let realEdgeData = processedData.realEdges;

  edgeData.push({
    id: "L-1",
    source: nodeData[0],
    target: ""
  });
  //Store original data
  SharedData.edgeData = edgeData;
  //Store real edges
  SharedData.realEdgeData = realEdgeData;

  SharedData.hierarchy = SharedData.createHierarchy(edgeData);

  // update and draw the tree
  SharedData.updateHierarchyVars(SharedData.hierarchy);

  SharedData.nodes = app.svgRootLayer.append("g")
      .attr("id", "nodes")
      .attr("cursor", "pointer")
      .attr("pointer-events", "all");

  SharedData.labels = app.svg.selectAll("#nodes");

  SharedData.advancedUpdate(SharedData.hierarchy);

  if (proofState === "full")
    show(false);
  else
    show();
}

function show(firstOnly = true){
  let i = 0, cls = "visible";
  for(i; i < SharedData.orderedElements.length; i++){
    d3.select("#N"+SharedData.orderedElements[i].data.source.id).classed(cls,true)
    if(firstOnly && i === 0)
      cls = "invisible";
  }
}

function processData(data) {
  // Compute edges
  let edgeData = [].map.call(data.querySelectorAll("edge"), d => {
    let edgeId = d.getAttribute("id");
    let edgeSource = d.getAttribute("source");
    let edgeTarget = d.getAttribute("target");

    return { id: edgeId, source: edgeSource, target: edgeTarget };
  });

  // Compute nodes
  let nodeData = [].map.call(data.querySelectorAll("node"), d => {
    let dataNodes, typeVar, elementVar, msElementVar, nlElementVar, idVar;
    let asserted = true;

    idVar = d.getAttribute("id");

    dataNodes = d.querySelectorAll("data");

    dataNodes.forEach(function (item) {
      if (item.getAttribute("key") === "type") {
        typeVar = item.textContent;
      }
      else if(item.getAttribute("key") === "nlelement"){
        nlElementVar = item.textContent;
      }
    });

    return { id: idVar, type: typeVar, nlelement:nlElementVar};
  });

  // Add the nodeData to the edgeData
  edgeData.forEach(function (d) {

    var sourceNode = nodeData.find(b => b.id === d.source);
    var targetNode = nodeData.find(b => b.id === d.target);

    d.source = sourceNode;
    d.target = targetNode;
  });

  // read realEdges to use them in them in the highlight
  let realEdgeData = [].map.call(data.querySelectorAll("realEdge"), d => {
    let edgeId = d.getAttribute("id");
    let edgeFakeSource = d.getAttribute("fakeSource");
    let edgeRealSource = d.getAttribute("realSource").split(",");
    let edgeTarget = d.getAttribute("target");

    return { id: edgeId, fakeSourceID: edgeFakeSource, realSourceID: edgeRealSource, targetID: edgeTarget };
  });

  return {
    nodes: nodeData,
    edges: edgeData,
    realEdges:realEdgeData
  };
}

dragElement(document.getElementById("navigation-container"));

function dragElement(element) {
  let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(element.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(element.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    element.onmousedown = dragMouseDown;
  }

  window.addEventListener("dragstart", (e) => dragMouseDown(e));

  function dragMouseDown(e) {
    if (app.padLock)
      return;
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    element.style.left = (element.offsetLeft - pos1) + "px";
    element.style.top = (element.offsetTop - pos2) + "px";

  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}